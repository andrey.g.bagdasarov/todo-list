package components.header

import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.h1
import react.dom.header
import react.dom.p

private class Header : RComponent<RProps, RState>() {
    override fun RBuilder.render() {
        header {
            h1 { +"Simple TODO App" }
            p { +"Please add to-dos item(s) through the input field" }
        }
    }
}


fun RBuilder.   header() = child(Header::class) {}