package components.todolist

import components.todoitem.todoItem
import react.RBuilder
import react.RComponent
import react.dom.div
import react.dom.title
import react.dom.ul
import state.AppState

class TodoList : RComponent<TodoListProps, AppState>() {

    override fun RBuilder.render() {
        div {
            ul {
                for (item in props.todos) {
                    todoItem(item)
                }
            }
        }
    }
}