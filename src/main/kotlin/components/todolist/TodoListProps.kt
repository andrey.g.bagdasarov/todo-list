package components.todolist

import objects.Todo
import react.RProps

interface TodoListProps : RProps {
    var todos: List<Todo>
}