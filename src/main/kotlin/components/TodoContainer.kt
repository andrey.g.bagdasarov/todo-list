package components

import components.header.header
import components.todolist.TodoList
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import objects.Todo
import react.RBuilder
import react.RComponent
import react.dom.div
import react.setState
import state.AppState

class TodoContainer : RComponent<TodoContainerProps, AppState>() {


    private fun initTodos(): List<Todo> {
        val todos = mutableListOf<Todo>()
        for (x in 0..2) {
            todos.add(Todo(x, "Memo:$x", false))
        }
        return todos
    }

    override fun AppState.init() {
        todos = listOf()
        val mainScope = MainScope()

        mainScope.launch {
            val initTodos = initTodos()
            setState {
                todos = initTodos
            }
        }
    }

    override fun RBuilder.render() {
        div {
            header()
            child(TodoList::class) {
                attrs.todos = state.todos
            }
        }
    }
}