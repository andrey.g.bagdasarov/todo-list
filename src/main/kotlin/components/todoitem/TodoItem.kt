package components.todoitem

import kotlinx.html.InputType
import kotlinx.html.js.onChangeFunction
import kotlinx.html.onChange
import objects.Todo
import react.RBuilder
import react.RComponent
import react.RProps
import react.dom.input
import react.dom.li
import state.AppState

interface TodoItemProps : RProps {
    var key: Int
    var todo: Todo
}

private class TodoItem : RComponent<TodoItemProps, AppState>() {
    override fun RBuilder.render() {
        li {
            input(type = InputType.checkBox) {
                attrs {
                    checked = props.todo.completed
                    onChangeFunction = {println("Clicked!")}
                }
            }
            +props.todo.title
        }
    }
}


fun RBuilder.todoItem(todo: Todo) = child(TodoItem::class) {
    attrs.todo = todo
}