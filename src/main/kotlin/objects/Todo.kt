package objects

data class Todo(var id: Int, var title: String, var completed: Boolean)