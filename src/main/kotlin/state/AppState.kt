package state

import objects.Todo
import react.RState

interface AppState : RState {
    var todos : List<Todo>
}